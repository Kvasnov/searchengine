﻿/*
 
Выполнил: Кваснов Б.А.

****************************************************
Задание:
Программа должна быть реализована на Windows Forms, Visual Studio 2017

Код программы должен быть выложен под git на bitbucket и открыт доступ на чтение репозитория.

Программа для поиска файлов по заданным критериям
Критерии:
- Стартовая директория (с которой начинается поиск)
- Шаблон имени файла
- Текст, содержащийся в файле

Введенные критерии не должны потеряться при перезапуске программы

Во время поиска нужно отображать какой файл обрабатывается в данный момент, количество обработанных файлов и прошедшее время.
Все найденные файлы отображать в виде дерева (как в левой части проводника)
Найденные файлы должны обновляться в реальном времени

Поиск нужно уметь остановить в любой момент и затем либо продолжить, либо начать новый
****************************************************

Алгоритм:
Для реализации отзывчивого пользовательского интерфейса использовался компонент BackgroundWorker.
В основном потоке реализован пользовательский интерфейс
    1) Кнопки Старт/Пауза, Сброс, Выбрать папку
    2) Текстовые поля Найти текст, Найти файл, Папка:
    3) Проводник
В дополнительном потоке реализован метод CreateDirectoryNode для поиска файлов и создания древа найденных файлов в проводнике:
    1) получение стартовой директории
    2) открытие папок по очереди
    3) добавление папки в ветку проводника (используется рекурсия)
    4) получение файлов по критерию "Шаблон имени файла"
    5) создание счетчиков обработанных и найденных файлов
    6) создание потока для чтения файлов
    7) получение файлов по критерию "Текст, содержащийся в файле"
    8) вызов события "изменение прогресса" для обновления в проводнике найденных файлов в реальном времени
    9) в событии "изменение прогресса" происходит:
        - проверка существования элемента в текущей ветке, если нет, то добавляет данный элемент
        - добавление новой ветки проводника
        - обновление строки "какой файл обрабатывается в данный момент"
При нажатии на кнопку Старт:
    1) Запускается поиск файлов и отображение найденных файлов в проводнике
    2) Счетчик времени работы приложения
    3) Сохранение критериев поиска в текстовый файл
    4) Изменение кнопки Старт на кнопку Пауза
    5) Текстовые поля и кнопка Выбрать папку становятся неактивными
    6) При нажатии кнопки после паузы продолжается поиск файлов (pauseEvent.Set())
    7) После завершения поиска, кнопка Старт становится неактивной
При нажатии на кнопку Пауза:
    1) Останавливается поиск файлов и отображение найденных файлов в проводнике (pauseEvent.Reset())
    2) Изменение кнопки Пауза на кнопку Старт
При нажатии на кнопку Сброс
    1) Сбрасываются результаты поиска, счетчик времени и тд.

*/

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SearchEngine
{
    public partial class SearchEngine : Form
    {
        private static readonly Stopwatch watch = new Stopwatch();
        string SettingsPath = Application.StartupPath + "\\settings.txt";
        int countFoundFiles = 0;
        int countProcessedFiles = 0;

        ManualResetEvent pauseEvent = new ManualResetEvent(true);


        public SearchEngine()
        {
            InitializeComponent();

            UpdateTime();

            InitializeSettings();

            labelFileName.Text = string.Empty;
            labelResult.Text = string.Empty;
        }

        private void buttonOpenDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog.SelectedPath;
                textBoxPath.Text = path;
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (buttonStart.Text == "Старт")
            {
                if ((textBoxPath.Text == null) || !Directory.Exists(textBoxPath.Text))
                {
                    labelResult.Text = "Пожалуйста, введите корректную стартовую директорию!";
                    return;
                }

                watch.Start();
                timer.Enabled = true;
                buttonStart.Text = "Пауза";

                pauseEvent.Set();

                if (!backgroundWorker.IsBusy) // действия при первом нажатии кнопки "Старт" (запуск поиска файлов)
                {
                    buttonReset.Enabled = true;
                    buttonOpenDirectory.Enabled = false;
                    textBoxTextInFile.Enabled = false;
                    textBoxFileName.Enabled = false;
                    textBoxPath.Enabled = false;
                    labelResult.Text = string.Empty;

                    SaveSettings();

                    backgroundWorker.RunWorkerAsync();
                }
            }
            else // после нажатия на кнопку паузы, надпись меняется на старт, время продолжает с остановленного месте
            {
                watch.Stop();
                timer.Enabled = false;
                buttonStart.Text = "Старт";

                pauseEvent.Reset();
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            ResetUI();

            if (backgroundWorker.IsBusy) // действия при нажатии кнопки "Стоп"
            {
                backgroundWorker.CancelAsync();
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateTime();
        }

        private void UpdateTime()
        {
            statusBarPanelStopwatch.Text = GetTimeString(watch.Elapsed);
        }

        private string GetTimeString(TimeSpan elapsed) // формат строки времени
        {
            string result = string.Empty;

            result = string.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                elapsed.Hours,
                elapsed.Minutes,
                elapsed.Seconds,
                elapsed.Milliseconds);

            return result;
        }

        private void SaveSettings() // сохранение критериев поиска
        {
            StreamWriter writer = new StreamWriter(SettingsPath);
            writer.WriteLine(textBoxPath.Text);
            writer.WriteLine(textBoxFileName.Text);
            writer.Write(textBoxTextInFile.Text);
            writer.Close();
        }

        private void InitializeSettings() // инициализация критериев последнего поиска после закрытия программы
        {
            if (File.Exists(SettingsPath))
            {
                StreamReader reader = new StreamReader(SettingsPath);
                textBoxPath.Text = reader.ReadLine();
                textBoxFileName.Text = reader.ReadLine();
                textBoxTextInFile.Text = reader.ReadLine();
                reader.Close();
            }

        }

        private void ResetUI()
        {
            buttonStart.Text = "Старт";
            buttonReset.Enabled = false;
            buttonStart.Enabled = true;
            buttonOpenDirectory.Enabled = true;
            textBoxTextInFile.Enabled = true;
            textBoxFileName.Enabled = true;
            textBoxPath.Enabled = true;

            watch.Reset();
            UpdateTime();

            labelFileName.Text = string.Empty;
            labelResult.Text = string.Empty;
            treeView1.Nodes.Clear();

            countFoundFiles = 0;
            countProcessedFiles = 0;
            statusBarPanelFoundFiles.Text = "0";
            statusBarPanelProcessedFiles.Text = "0";

            pauseEvent.Set();
        }
        // создание проводника по найденным файлам
        protected TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeNode(directoryInfo.Name); // получение стартовой директории

            foreach (var directory in directoryInfo.GetDirectories()) // открытие папок по очереди
            {
                directoryNode.Nodes.Add(CreateDirectoryNode(directory)); // добавление папки в ветку проводника (используется рекурсия)
            }

            int counterFoundFiles = 0;
            int counterProcessedFiles = 0;

            string pattern = (textBoxFileName.Text.Length == 0) ? "*" : textBoxFileName.Text + "*";

            foreach (var filePath in directoryInfo.GetFiles(pattern)) // получение файлов по критерию "Шаблон имени файла"
            {
                pauseEvent.WaitOne(Timeout.Infinite); // при нажатии на кнопку "Пауза" вызывается ожидание

                if (backgroundWorker.CancellationPending) // при нажатии на кнопку "Сброс" происходит выход из приложения
                {
                    return directoryNode;
                }

                // создание потока для чтения файлов
                FileStream fileStream = filePath.OpenRead();
                StreamReader file = new StreamReader(fileStream);
                // счетчик проверенных файлов
                counterProcessedFiles = ++countProcessedFiles;
                statusBarPanelProcessedFiles.Parent.Invoke(new Action(() => statusBarPanelProcessedFiles.Text = counterProcessedFiles.ToString()));

                if (file.ReadToEnd().IndexOf(textBoxTextInFile.Text) >= 0) // получение файлов по критерию "Текст, содержащийся в файле"
                {
                    // счетчик найденных файлов
                    counterFoundFiles = ++countFoundFiles;
                    statusBarPanelFoundFiles.Parent.Invoke(new Action(() => statusBarPanelFoundFiles.Text = counterFoundFiles.ToString()));
                    int p = 0; // значение равно нулю, так как индикатор прогресса не используется
                    object param = filePath.FullName; // запись полного пути файла в дополнительный параметр для события "изменение прогресса"
                    backgroundWorker.ReportProgress(p, param); // вызов события "изменение прогресса" для обновления в проводнике найденных файлов в реальном времени
                }
                file.Close();
                fileStream.Close();
            }
            return directoryNode;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var rootDirectoryInfo = new DirectoryInfo(textBoxPath.Text);

            var directoryNode = CreateDirectoryNode(rootDirectoryInfo);

            if (backgroundWorker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
        }
        // Добавление файла в проводник
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!backgroundWorker.CancellationPending)
            {
                string path = e.UserState.ToString();
                string[] splitPath = path.Split('\\');
                var node = treeView1.Nodes;

                for (int i = 0; i < splitPath.Length; ++i)
                {
                    if (node[splitPath[i]] == null) // проверка существования элемента в текущей ветке, если нет, то добавляет данный элемент
                    {
                        node.Add(splitPath[i], splitPath[i]);
                    }
                    node = node[splitPath[i]].Nodes; // добавление новой ветки проводника 
                }
                // обновление строки "какой файл обрабатывается в данный момент"
                labelFileName.Text = Path.GetFileName(path);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                labelResult.Text = e.Error.Message;
            }
            else if (e.Cancelled)
            {
                ResetUI();
                labelResult.Text = "Выполнен сброс поиска";
            }
            else
            {
                watch.Stop();
                buttonStart.Text = "Старт";
                buttonReset.Enabled = true;
                buttonStart.Enabled = false;

                labelResult.Text = "Приложение закончило поиск!";
            }
        }
    }
}