﻿namespace SearchEngine
{
    partial class SearchEngine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxTextInFile = new System.Windows.Forms.TextBox();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.buttonOpenDirectory = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.statusBarPanelStopwatch = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanelProcessedFilesText = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanelProcessedFiles = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanelFoundFilesText = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanelFoundFiles = new System.Windows.Forms.StatusBarPanel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.labelFileName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.labelResult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelStopwatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelProcessedFilesText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelProcessedFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelFoundFilesText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelFoundFiles)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxTextInFile
            // 
            this.textBoxTextInFile.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTextInFile.Location = new System.Drawing.Point(114, 34);
            this.textBoxTextInFile.Name = "textBoxTextInFile";
            this.textBoxTextInFile.Size = new System.Drawing.Size(563, 23);
            this.textBoxTextInFile.TabIndex = 0;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxFileName.Location = new System.Drawing.Point(114, 66);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(563, 23);
            this.textBoxFileName.TabIndex = 1;
            // 
            // textBoxPath
            // 
            this.textBoxPath.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPath.Location = new System.Drawing.Point(114, 98);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(563, 23);
            this.textBoxPath.TabIndex = 2;
            // 
            // buttonOpenDirectory
            // 
            this.buttonOpenDirectory.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOpenDirectory.Location = new System.Drawing.Point(683, 97);
            this.buttonOpenDirectory.Name = "buttonOpenDirectory";
            this.buttonOpenDirectory.Size = new System.Drawing.Size(130, 25);
            this.buttonOpenDirectory.TabIndex = 3;
            this.buttonOpenDirectory.Text = "Выбрать папку";
            this.buttonOpenDirectory.UseVisualStyleBackColor = true;
            this.buttonOpenDirectory.Click += new System.EventHandler(this.buttonOpenDirectory_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Enabled = false;
            this.buttonReset.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonReset.Location = new System.Drawing.Point(229, 200);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(100, 40);
            this.buttonReset.TabIndex = 4;
            this.buttonReset.TabStop = false;
            this.buttonReset.Text = "Сброс";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStart.Location = new System.Drawing.Point(114, 200);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(100, 40);
            this.buttonStart.TabIndex = 5;
            this.buttonStart.TabStop = false;
            this.buttonStart.Text = "Старт";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // statusBar1
            // 
            this.statusBar1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusBar1.Location = new System.Drawing.Point(0, 536);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanelStopwatch,
            this.statusBarPanelProcessedFilesText,
            this.statusBarPanelProcessedFiles,
            this.statusBarPanelFoundFilesText,
            this.statusBarPanelFoundFiles});
            this.statusBar1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.statusBar1.ShowPanels = true;
            this.statusBar1.Size = new System.Drawing.Size(870, 30);
            this.statusBar1.TabIndex = 6;
            this.statusBar1.Text = "statusBar1";
            // 
            // statusBarPanelStopwatch
            // 
            this.statusBarPanelStopwatch.Name = "statusBarPanelStopwatch";
            this.statusBarPanelStopwatch.Text = "00:00:00.000";
            this.statusBarPanelStopwatch.Width = 200;
            // 
            // statusBarPanelProcessedFilesText
            // 
            this.statusBarPanelProcessedFilesText.Name = "statusBarPanelProcessedFilesText";
            this.statusBarPanelProcessedFilesText.Text = "Обработано:";
            this.statusBarPanelProcessedFilesText.Width = 85;
            // 
            // statusBarPanelProcessedFiles
            // 
            this.statusBarPanelProcessedFiles.Name = "statusBarPanelProcessedFiles";
            this.statusBarPanelProcessedFiles.Text = "0";
            this.statusBarPanelProcessedFiles.Width = 150;
            // 
            // statusBarPanelFoundFilesText
            // 
            this.statusBarPanelFoundFilesText.Name = "statusBarPanelFoundFilesText";
            this.statusBarPanelFoundFilesText.Text = "Найдено:";
            this.statusBarPanelFoundFilesText.Width = 65;
            // 
            // statusBarPanelFoundFiles
            // 
            this.statusBarPanelFoundFiles.Name = "statusBarPanelFoundFiles";
            this.statusBarPanelFoundFiles.Text = "0";
            this.statusBarPanelFoundFiles.Width = 150;
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeView1.Location = new System.Drawing.Point(0, 274);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(870, 256);
            this.treeView1.TabIndex = 7;
            this.treeView1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(111, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(327, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "В данный момент обрабатывается файл:";
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFileName.Location = new System.Drawing.Point(111, 170);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(61, 16);
            this.labelFileName.TabIndex = 9;
            this.labelFileName.Text = "FileName";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 23);
            this.label3.TabIndex = 10;
            this.label3.Text = "Найти текст:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(0, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 23);
            this.label4.TabIndex = 11;
            this.label4.Text = "Маска файлов:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(24, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 23);
            this.label5.TabIndex = 12;
            this.label5.Text = "Папка:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timer
            // 
            this.timer.Interval = 25;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelResult.Location = new System.Drawing.Point(12, 255);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(43, 16);
            this.labelResult.TabIndex = 13;
            this.labelResult.Text = "Result";
            // 
            // SearchEngine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 566);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonOpenDirectory);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.textBoxTextInFile);
            this.Name = "SearchEngine";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelStopwatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelProcessedFilesText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelProcessedFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelFoundFilesText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelFoundFiles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTextInFile;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Button buttonOpenDirectory;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.StatusBarPanel statusBarPanelStopwatch;
        private System.Windows.Forms.StatusBarPanel statusBarPanelProcessedFilesText;
        private System.Windows.Forms.StatusBarPanel statusBarPanelProcessedFiles;
        private System.Windows.Forms.StatusBarPanel statusBarPanelFoundFilesText;
        private System.Windows.Forms.StatusBarPanel statusBarPanelFoundFiles;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label labelResult;
    }
}

